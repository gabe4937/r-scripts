#radiation wavelength
lambda = 1.54051
#angles measured
theta <- c(68.135, 68.215, 54.410, 54.210)
#set up vector to contain d-spacings
d <- c(0, 0, 0, 0)
#calculate d-spacings
for (i in 1:length(theta)) {
    d[i] = lambda / (2 * sin(theta[i] * pi / 180))
    print( d[i] ) 
}
#calculate change in d from 40 - 140 for both angles
print((d[2] - d[1])/d[1])
print((d[4] - d[3])/d[3])